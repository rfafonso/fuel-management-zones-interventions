# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 21:07:21 2019

@author: Ricardo
"""
import os,sys
import pandas as pd
import traceback
import numpy as np
from osgeo import gdal,ogr
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from timeit import default_timer as timer
import math
import datetime

def date_to_day(date):
    date = datetime.datetime.strptime(date, '%Y%m%d')
    new_year_day = datetime.datetime(year=date.year, month=1, day=1)
    return (date - new_year_day).days + 1

def detect_intervention(fgc_dir, dates, local, y_fora, y_dentro):    
    
    size = int(y_dentro.shape[0])
    dates_count = 20

    cluster = y_dentro[0:size,1]
    
    print(y_dentro.shape[0])
    print(y_fora.shape[0])
    
    interv = {}
    start = timer()    
    
    found = False
    for i in range(0, size, dates_count):
#        if np.unique(y_fora[i:(i+dates_count),2]).shape[0] > 1 and found == False:
#            found = True
#            print("Problema: Dados em falta")
#            print(y_fora[i:(i+dates_count),:])
            
        if y_fora[i:(i+dates_count),2].shape[0] == y_dentro[i:(i+dates_count),2].shape[0] and cluster[i] in local  :
            deltas = y_fora[i:(i+dates_count),2].astype(float) - y_dentro[i:(i+dates_count),2].astype(float)
            
            for j in range(0, deltas.shape[0]):
                
                if j+1 < deltas.shape[0]:
                    val = deltas[j+1] - deltas[j]
                    
                    if cluster[i] == "c3433" :
                            print( dates[j+1],"-",dates[j], " = ", val)
                            
#                    if val > 0.10 and cluster[i] not in interv:
                    if val > 0.1 :
                        interv[cluster[i]] =  str(dates[j])+"-"+str(dates[j+1])
                        
#                        print(str(cluster[i]) + "limpo na data" + dates[j])
    
    duration = timer() - start     
    print("duration: ",duration)                 
    print("Adding to file!")
    
    file = fgc_dir+"/estradas/polygons.shp"
    clusters_shp = ogr.Open(file)
    clusters = clusters_shp.GetLayer()
    
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(fgc_dir+"/estradas/intervention_date_estimation.shp")    
    final_lyr = ds.CreateLayer('temp', clusters.GetSpatialRef(),ogr.wkbMultiPolygon  ) 
    
    newfield = ogr.FieldDefn("INTER_DATE", ogr.OFTString)
    final_lyr.CreateField(newfield)
    newfield = ogr.FieldDefn("CLUSTER_ID", ogr.OFTString)
    final_lyr.CreateField(newfield)
    
#    print(interv.keys())
    for feature in clusters:
        geom = feature.GetGeometryRef()
        
        i_cluster_id = feature.GetFieldIndex("CLUSTER_ID")
        cluster_id = str(int(float(feature.GetFieldAsString(i_cluster_id)  )))
#        print(cluster_id)
        key = "c"+cluster_id
        
        if key in interv:
            
            new_feat = ogr.Feature(final_lyr.GetLayerDefn())
            new_feat.SetFrom(feature)
            new_feat.SetGeometry(geom)
            new_feat.SetField("INTER_DATE",interv[key])
            new_feat.SetField("CLUSTER_ID",cluster_id)
            
            final_lyr.CreateFeature(new_feat)
            
            
    cluster_shp = None
    return interv 


if __name__ == '__main__': 
    if len(sys.argv) != 3 :
        print("Missing arguments! Command usage:")
        print("  python EstimateIntervention.py '<path_to_fgci_directory>' \n")

        sys.exit()
    
    fgc_dir = sys.argv[1]
    
    
#    fgc_dir = "../../exp/series/clusters/data"
    
    #O shapefile que resulta desta estimativa, apenas tem estes clusters/seccoes (local1 e local2) com a data estimada
    local1 = ["c1142","c1329","c1436","c2176","c2739","c2794","c2837","c290","c3427","c3833","c4899","c4900","c4901","c5146","c628"] # intrevencionadas entre 15/05 e 19/06
    
    local2 = ["c1762","c1785","c2571","c2645","c2988","c3428","c4082","c4310","c4702","c4902","c5149","c570","c870"] # intervencionadas entre 24/06 e 29/07
    
    
    df2=pd.read_csv(fgc_dir+"/estradas/dados_estradas_s2.csv", sep=',', engine="python")
    
    y_dentro = df2.loc[df2["position"] == "in", ["date","cluster","ndvi"]].values
    y_fora = df2.loc[df2["position"] == "out", ["date","cluster","ndvi"]].values
    
    
    dates = np.unique(df2["date"].values)
    
    detect_intervention(fgc_dir, dates, local1, y_fora, y_dentro)    
    
    


















