# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 21:42:16 2019

@author: Ricardo
"""

from __future__ import print_function
import cv2
from osgeo import gdal
from osgeo import osr
import numpy as np
from timeit import default_timer as timer
from numba import jit
import math
import re
import os,sys
 
 
MAX_FEATURES = 100
GOOD_MATCH_PERCENT = 0.5
gdal.UseExceptions()

ref_images_path = "E:/rafon/Documents/storage/Dados Tese/Imagem de Referencia_backup/Sentinel-2/"


def getWarpMatrix(newFilename,refFilename):
  im1 = cv2.imread(newFilename, cv2.IMREAD_COLOR)  
  im2= cv2.imread(refFilename, cv2.IMREAD_COLOR)
  
      # Convert images to grayscale
  im1Gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
  im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)
   
  # Detect ORB features and compute descriptors.
  orb = cv2.ORB_create(MAX_FEATURES)
  keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
  keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)
   
  # Match features.
  matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
  matches = matcher.match(descriptors1, descriptors2, None)


  # Sort matches by score
  matches.sort(key=lambda x: x.distance, reverse=False)
 
  # Remove not so good matches
  numGoodMatches = int(len(matches) * GOOD_MATCH_PERCENT)
  matches = matches[:numGoodMatches]
   
  # Extract location of good matches
  points1 = np.zeros((len(matches), 2), dtype=np.float32)
  points2 = np.zeros((len(matches), 2), dtype=np.float32)
  
  for i, match in enumerate(matches):
   points1[i, :] = keypoints1[match.queryIdx].pt
   points2[i, :] = keypoints2[match.trainIdx].pt  



  imMatches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, None)
  cv2.imwrite("matches.jpg", imMatches)

#  test,mm = cv2.findFundamentalMat(points1, points2)
#  pt1 = np.reshape(points1,(1,2500,2))
#  pt2 = np.reshape(points2,(1,2500,2))
#  points1, points2 =  cv2.correctMatches(test, pt1, pt2)
#  print(test)
   
   
#  # Find homography
#  h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)
#  print(h)
#  # Use homography
#  height, width, channels = im2.shape
#  im1Reg = cv2.warpPerspective(im1, h, (width, height))
  
  warp_matrix = np.eye(2, 3, dtype=np.float32)
  (warp_matrix, mask) = cv2.estimateAffine2D(points2, points1, warp_matrix, maxIters=10000, confidence=0.99)

  return warp_matrix




def align_image(refFile, imFile, warp_matrix , out):

#  height, width, channels = im2.shape
#  im1Reg = cv2.warpPerspective(im1, h, (width, height))
  ref = gdal.Open(refFile)
  img = gdal.Open(imFile)  
  
  #######  
  #warp_matrix = np.eye(2, 3, dtype=np.float32)
  print(warp_matrix)
  
  #fica a zero pois não é necessário rotações na imagem
  warp_matrix[0,1] = 0.00
  warp_matrix[1,0] = 0.00
  warp_matrix[0,0] = 1.00
  warp_matrix[1,1] = 1.00
  
  #deslocamento a ser feito nas coordenadas da imagem
  dx = -warp_matrix[0,2]*10.0
  dy = warp_matrix[1,2]*10.0    
  

  driver = gdal.GetDriverByName("GTiff")
  dst_ds = driver.CreateCopy(out, img, strict=0)
  
 
  geo = ref.GetGeoTransform()
  new_geo = ([geo[0]+dx, geo[1], geo[2],geo[3]+dy,geo[4],geo[5]])
  dst_ds.SetGeoTransform(new_geo)
  dst_ds.SetProjection(ref.GetProjection())
#  gdal.Warp('../../exp/aligned_20180425.tif',dst_ds,dstSRS='EPSG:32629')

  dst_ds = None
      





def alignImages(im1, im2):
 
  # Convert images to grayscale
  im1Gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
  im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)
   
  # Detect ORB features and compute descriptors.
  orb = cv2.ORB_create(MAX_FEATURES)
  keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
  keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)
   
  # Match features.
  matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
  matches = matcher.match(descriptors1, descriptors2, None)


  # Sort matches by score
  matches.sort(key=lambda x: x.distance, reverse=False)
 
  # Remove not so good matches
  numGoodMatches = int(len(matches) * GOOD_MATCH_PERCENT)
  matches = matches[:numGoodMatches]
   
  # Extract location of good matches
  points1 = np.zeros((len(matches), 2), dtype=np.float32)
  points2 = np.zeros((len(matches), 2), dtype=np.float32)
  
  for i, match in enumerate(matches):
   points1[i, :] = keypoints1[match.queryIdx].pt
   points2[i, :] = keypoints2[match.trainIdx].pt  


#  test,mm = cv2.findFundamentalMat(points1, points2)
#  pt1 = np.reshape(points1,(1,2500,2))
#  pt2 = np.reshape(points2,(1,2500,2))
#  points1, points2 =  cv2.correctMatches(test, pt1, pt2)
#  print(test)
   
   
#  # Find homography
#  h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)
#  print(h)
#  # Use homography
  height, width, channels = im2.shape
#  im1Reg = cv2.warpPerspective(im1, h, (width, height))
  
#  print(channels)
  
  warp_matrix = np.eye(2, 3, dtype=np.float32)
  (warp_matrix, mask) = cv2.estimateAffine2D(points2, points1, warp_matrix, maxIters=10000, confidence=0.99)
  
  #######  
  #warp_matrix = np.eye(2, 3, dtype=np.float32)
  im_aligned = np.zeros((height,width,3), dtype=np.uint8 )
  print("warp matrix:")
  print(warp_matrix)
  
  #fica a zero pois não é necessário rotações na imagem
  warp_matrix[0,1] = 0.00
  warp_matrix[1,0] = 0.00
  warp_matrix[0,0] = 1.00
  warp_matrix[1,1] = 1.00
  
  #deslocamento a ser feito nas coordenadas da imagem
  dx = -warp_matrix[0,2]*10.0
  dy = warp_matrix[1,2]*10.0
  
#  warp_matrix[0,2] = 0.00
  #warp_matrix[1,2] = -1.8
  
  #Alinha a propria imagem (resulta em pixeis a preto nas extremidades)
  for i in range(0,channels) :  
      im_aligned[:,:,i] = cv2.warpAffine(im1[:,:,i], warp_matrix, (height, width), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP);
     
      #im_aligned[:,:,i]= warp(i, warp_matrix, im1)
      #print(warp_matrix)
      
  return im_aligned, dx,dy
 


def get_ref_image(folder):
    
#    ref_images_path = "E:/rafon/Documents/storage/Dados Tese/Imagem de Referencia_backup/Sentinel-2/"
    product = re.findall(r"[\\/]{1}S2.*\.SAFE", str(folder))[0][1:-1]
    tile = product.split("_")[5]
    
    for subdir, dirs, files in os.walk(ref_images_path):
        for d in dirs:
            if ".SAFE" in d and tile in d :
                ref_prod = ref_images_path + d 
    
    for subdir, dirs, files in os.walk(ref_prod):
        for file in files:
            file_path = subdir+'/'+file
            if "TCI_10m" in file and (file.endswith(".tif") or file.endswith(".jp2")):    
                ref_10m = file_path
            if "TCI_20m" in file and (file.endswith(".tif") or file.endswith(".jp2")):    
                ref_20m = file_path

    return ref_10m, ref_20m

                
def align_all_images(folder): 
    
    refImage_10m_path , refImage_20m_path  = get_ref_image(folder)
      
    #Obtem a matriz de transformação baseada na imagem tci
    for subdir, dirs, files in os.walk(folder):
        for file in files:
            file_path = subdir+'/'+file  
            if "TCI_10m" in file and (file.endswith(".tif") or file.endswith(".jp2")):
                print("A calcular a matriz: ", file )
                warp_matrix = getWarpMatrix(file_path, refImage_10m_path)  

    #aplica a matriz de transformação a todas as outras imagens 
    for subdir, dirs, files in os.walk(folder):
        for file in files:
            
            file_path = subdir+'/'+file
            
            if file.endswith(".tif") or file.endswith(".jp2"):
                if "_10m" in file:
                    align_image(refImage_10m_path,file_path, warp_matrix, subdir+"/aligned_"+file)
                else:    
                    align_image(refImage_20m_path,file_path, warp_matrix, subdir+"/aligned_"+file)


def align_all_products(folder): 
    for subdir, dirs, files in os.walk(folder):
        for d in dirs:
            if ".SAFE" in d:
                curr_dir = subdir+"/"+d
                print("Alignig product: "+curr_dir)
                align_all_images(curr_dir)
        
        
    
if __name__ == '__main__':
   
    if len(sys.argv) != 2 :
        print("Missing arguments! Command usage:\n")
        print("   python ProcessFGCFiles.py '<path_to_sentinel2_images_folder>' ")
        sys.exit()
    
    folder = sys.argv[1]
    
    start = timer()
  
    align_all_products(folder)

    duration = timer()-start
    print("Duration: ",duration)

  
  
  
  
  