# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""



import os
from osgeo import gdal, ogr,osr


def resample(infile,outfile):

    ds = gdal.Warp(outfile,infile, 
                   format='GTiff',
                   xRes = 10.0, yRes= -10.0,
                   targetAlignedPixels =False,
                   resampleAlg ="lanczos",
                   multithread =True,
                   dstNodata = 0)
    ds=None



def resample_all(path):
    for subdir, dirs, files in os.walk(path):

        for file in files:
            
            file_path = subdir+'/'+file
             
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B05" in file:
                outfile = file_path.replace("20m","10m")
                resample(file_path,outfile)
                
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B06" in file:
                outfile = file_path.replace("20m","10m")
                resample(file_path,outfile)
                
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B07" in file:
                outfile = file_path.replace("20m","10m")
                resample(file_path,outfile)
            
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B11" in file:
                outfile = file_path.replace("20m","10m")
                resample(file_path,outfile)    
                
                
                
def resample_all_products(folder): 
    for subdir, dirs, files in os.walk(folder):
        for d in dirs:
            if ".SAFE" in d:
                curr_dir = subdir+"/"+d
                print("Resampling product: "+curr_dir)
                resample_all(curr_dir)                
                

imRE1 = "../../exp/S2B_MSIL2A_20180515T112109_N0207_R037_T29SND_20180515T135621.SAFE/GRANULE/L2A_T29SND_A006212_20180515T112116/IMG_DATA/R20m/T29SND_20180515T112109_B05_20m.jp2"
imRE2 = "../../exp/S2B_MSIL2A_20180515T112109_N0207_R037_T29SND_20180515T135621.SAFE/GRANULE/L2A_T29SND_A006212_20180515T112116/IMG_DATA/R20m/T29SND_20180515T112109_B06_20m.jp2"
imRE3 = "../../exp/S2B_MSIL2A_20180515T112109_N0207_R037_T29SND_20180515T135621.SAFE/GRANULE/L2A_T29SND_A006212_20180515T112116/IMG_DATA/R20m/T29SND_20180515T112109_B07_20m.jp2"



resample_all_products("../../temp/")

#resample_all("../Cardigos/Sentinel-2/S2B_MSIL2A_20181231T112459_N0211_R037_T29SND_20181231T122356.SAFE")
