# -*- coding: utf-8 -*-
"""
Created on Sat Aug 31 11:23:40 2019

@author: Ricardo
"""

import DataExtration
import numpy as np
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
from sklearn import preprocessing



def plot_metrics(thres, results_0, results_1, index):
    
    plt.figure(figsize=(8,6))
#    plt.title("Secções Intervencionadas - "+index)
    plt.title("Secções Intervencionadas")
    plt.plot(thres,results_1[:,1], 'y', label="Precisão") 
    plt.plot(thres,results_1[:,2], 'b', label="Recall") 
    plt.plot(thres,results_1[:,3], 'g', label="F1") #F1
    #    plt.plot(rand_ix[:,0],rand_ix[:,6], 'm', label="Silhouette") #silhouette_score
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.xlabel('limite (l)', fontsize=13)
    plt.ylabel('Valor', fontsize=13)
    #    plt.savefig(title+".png", dpi=200,bbox_inches='tight')
    plt.show
    
    plt.figure(figsize=(8,6))
    plt.title("Secções não Intervencionadas")
    plt.plot(thres,results_0[:,1], 'y', label="precision") 
    plt.plot(thres,results_0[:,2], 'b', label="recall") 
    plt.plot(thres,results_0[:,3], 'g', label="F1") #F1
    #    plt.plot(rand_ix[:,0],rand_ix[:,6], 'm', label="Silhouette") #silhouette_score
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.xlabel('limite (l)', fontsize=13)
    plt.ylabel('Valor', fontsize=13)
    #    plt.savefig(title+".png", dpi=200,bbox_inches='tight')
    plt.show


def classify(my_data,threshold):

    labels = my_data[:,0] >= threshold
    
    return labels.astype(int)



def cross_validation(my_data):
    thres = np.arange(0.04 , 0.65, 0.02)
    results_0 = []
    results_1 = []
    
    for i in thres:
        pred = classify(my_data, i)
    #    print(classification_report(labels[:,1],pred))
        
        scores = classification_report(labels[:,1],pred,output_dict=True)
    
        results_0.append( (i,scores['0.0']["precision"] ,scores['0.0']["recall"] , scores['0.0']["f1-score"]))
        results_1.append( (i,scores['1.0']["precision"] ,scores['1.0']["recall"] , scores['1.0']["f1-score"]))
        
    
    results_0 = np.asarray(results_0)
    results_1 = np.asarray(results_1)
  
    return results_0, results_1
    



if __name__ == '__main__':

    fgci_dir = "../../exp/series/clusters/data/"
    fgci_type = "estradas"
    
    
    gt, my_data, labels = DataExtration.get_data(fgci_dir+fgci_type, fgci_type, 1,"s2")
    
    features_names = []
    
    for feat in my_data.columns:
        if "ndvi" not in feat and "ndwi" not in feat and "savi" not in feat:
            
            if "min_diff_" in feat :
                features_names.append(feat)
        else:
            if "max_diff_" in feat :
                features_names.append(feat)
            
    
    
    
    filtered_data = my_data.loc[:, features_names]
    
    
    for feat in features_names:
        
        thres = np.arange(0.04 , 0.65, 0.02)
        
        new_data = filtered_data.loc[:,[feat]].values
        
        if "ndvi" not in feat and "ndwi" not in feat and "savi" not in feat:
            max_val = np.quantile(new_data, 0.98)
            min_val = np.quantile(new_data, 0.02)
            new_data = (new_data - min_val) / (max_val - min_val)
        
    #    print(feat, "  max:",np.amax(new_data), "  min:", np.amin(new_data))
        
        results_0, results_1 = cross_validation(new_data)
    
        print(feat)
        best_entry = np.argmax(results_1[:,3]) # f1-score
        print("      threshold: ", results_1[best_entry,0], " f1-score: ")
        print("                 Interv: ", results_1[best_entry,3] )
        print("                 Não Interv: ", results_0[np.argmax(results_0[:,3]),3],"thres: ",results_0[np.argmax(results_0[:,3]),0] )
        plot_metrics(thres, results_0, results_1, feat)





