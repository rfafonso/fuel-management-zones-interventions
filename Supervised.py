# -*- coding: utf-8 -*-
"""
Created on Sun Jun 16 17:39:41 2019

@author: Ricardo
"""

from osgeo import ogr, gdal
from sklearn.cluster import KMeans
import itertools
import ast
from sklearn.metrics import precision_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import cohen_kappa_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import KFold
from sklearn.model_selection  import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
from sklearn.neural_network import MLPClassifier
from sklearn import svm
from scipy.stats import uniform
from timeit import default_timer as timer
from sklearn.preprocessing import MinMaxScaler

import matplotlib.pyplot as plt
import os, sys
import numpy as np
import pandas as pd
import DataExtration

import xgboost as xgb
import csv
import warnings

warnings.filterwarnings("ignore",  category=DeprecationWarning)



def get_model(name):
    model = None
    if name == "RF":
         model = RandomForestClassifier(random_state=0, class_weight ="balanced", n_jobs=-1)
    elif name == "SVM":
        model = svm.SVC( class_weight = "balanced")
    elif name == "XGB":
         model = xgb.XGBClassifier(silent=False, objective='binary:logistic', predictor="cpu_predictor",tree_method="gpu_hist")
    elif name == "KNN":
        model = KNeighborsClassifier(n_jobs =-1)
    elif name == "NB":    
        model = GaussianNB()
    elif name == "MLP":    
        model = MLPClassifier(random_state=1)
    
    return model



def report(results, n_top=1):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                  results['mean_test_score'][candidate],
                  results['std_test_score'][candidate]))
            print("Mean train score: {0:.3f} (std: {1:.3f})".format(
                  results['mean_train_score'][candidate],
                  results['std_train_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")
            if i == 1:
                best = results['params'][candidate]
    
    return best 
   

def params_search(model, parameters, x,y):

    rnd_search = RandomizedSearchCV(model, param_distributions=parameters, scoring='f1_macro',random_state=1, n_iter=50, cv=5, iid=False, return_train_score=True)
#    rnd_search = RandomizedSearchCV(model, param_distributions=parameters, random_state=3, n_iter=200, cv=4, iid=False)
    rnd_result = rnd_search.fit(x, y)
    res = rnd_result.cv_results_
    params = report(res)
    
    print("mean_score_time: ",res["mean_score_time"][rnd_result.best_index_])
    print("mean_fit_time: ",res["mean_fit_time"][rnd_result.best_index_])
    
    params = res['params'][rnd_result.best_index_]
    
    return params



def test_RF(x,y,dataset):
    
    model = get_model("RF")
    
    parameters = dict(n_estimators=[100,200,300,400,500,600], 
                      max_depth=[8,10,12,14,16,18,20,22,24], 
                      max_features= uniform(0.05, 0.94),
#                      min_weight_fraction_leaf = uniform(0.0,0.45),
#                      min_impurity_decrease = uniform(0.0,1.0),
                      min_samples_leaf = [1,2,3,4,5],
                      min_samples_split =[2,3,4,5,6],
                      criterion = ["gini","entropy"]
                      )
      
    best_params = params_search(model, parameters, x,y)
    
    return best_params



def test_XGB(x,y,dataset):
    
    model = get_model("XGB")
    
    parameters = dict(scale_pos_weight=uniform(0,2), 
                      learning_rate=uniform(0.001, 0.65),  
                      colsample_bytree = uniform(0.05,0.94),
                      subsample = uniform(0.01, 1.0),
                      n_estimators=[200,300,400,500,600], 
                      reg_alpha = uniform(0.01, 1.0),
                      max_depth=[2,4,6,8,10,12,14,16], 
                      gamma= uniform(0.1, 10), 
                      )

    
    best_params = params_search(model, parameters, x,y)


    return best_params


def test_SVM(x,y,dataset):
    model = get_model("SVM")
    
    parameters = dict(kernel=["rbf","sigmoid"],
                      C = uniform(0.01,600),
                      gamma = uniform(0.01,600),
                      degree=[2,3,4,5]
                      )

    
    best_params = params_search(model, parameters, x,y)
    

    return best_params


def test_KNN(x,y,dataset):
    model = get_model("KNN")
    parameters = dict(n_neighbors = [2,4,6,8,10,12,14],
                      weights = ["uniform", "distance"],
                      algorithm = ["auto", "ball_tree", "kd_tree"],
                      leaf_size = [20,25,30,35,40,45],
                      p = [1,2,3]                     
                      )
    
    best_params = params_search(model, parameters, x,y)
    

    
    return best_params


def formatFloat(value):
    return "{0:.3f}".format(value)

def calc_metrics(pred,labels):
    accuracy = accuracy_score(labels,pred)
    precision = precision_score(labels, pred)
    recall = recall_score(labels,pred)
    mae = mean_absolute_error(labels,pred)
    F1 = f1_score(labels,pred, average ="macro")
    
    return accuracy, precision, recall, mae, F1    
    
    
def plot_indexes(x,y,title):   
    plt.figure(figsize=(8,6))
    plt.title(title)
    plt.plot(x,y[:,0], 'r', label="accuracy")
    plt.plot(x,y[:,1], 'y', label="precision") 
    plt.plot(x,y[:,2], 'b', label="recall") 
    plt.plot(x,y[:,3], 'k', label="mean_absolute_error") 
    plt.plot(x,y[:,4], 'g', label="F1") #F1
#    plt.plot(rand_ix[:,0],rand_ix[:,6], 'm', label="Silhouette") #silhouette_score
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#    plt.savefig(title+".png", dpi=200,bbox_inches='tight')
    plt.show


def cross_validation(features_names,classifier, X_r,X_t,Y_r,Y_t, dataset, all_params, force_search):
    
    start = timer()
    
    if classifier == "RF":
        if "RF" not in all_params or force_search:
            print("Searching for Hyperparametres...")
            params = test_RF(X_r, Y_r[:,1],dataset)
        else:
            params = all_params["RF"]
        model = get_model("RF")
        
    elif classifier == "SVM":
        if "SVM" not in all_params or force_search:
            print("Searching for Hyperparametres...")
            params = test_SVM(X_r, Y_r[:,1],dataset)
        else:
            params = all_params["SVM"]
        model = get_model("SVM")
        
    elif classifier == "XGB":
        if "XGB" not in all_params or force_search:
            print("Searching for Hyperparametres...")
            params = test_XGB(X_r, Y_r[:,1],dataset)
        else:
            params = all_params["XGB"]
        model = get_model("XGB")
        
    elif classifier == "KNN":
        if "KNN" not in all_params or force_search:
            print("Searching for Hyperparametres...")
            params = test_KNN(X_r, Y_r[:,1],dataset)
        else:
            params = all_params["KNN"]
        model = get_model("KNN")
      
    duration = timer() - start
    print("Training duration: ", duration)

    model.set_params(**params)
    rf = model.fit(X_r,Y_r[:,1].ravel())
    l = rf.predict(X_t)
    
#    DataExtration.save_cluster_classification(pred, data, "Best_linhas_estatico1209_xgb.shp", "temp_linhas")   
    
    scores = classification_report(Y_t[:,1],l,output_dict=True)
  
    print(classification_report(Y_t[:,1],l))
    print("Kappa =", cohen_kappa_score(Y_t[:,1],l))
    
#    if classifier == "XGB":
#        featureScores = pd.Series(model.feature_importances_, index=features_names)
#        featureScores.nlargest(25).plot(kind='barh')
#        plt.show()
    
    
    return np.concatenate([Y_t[:,0][:,None],l[:,None]],axis=1).astype(int), scores, params


def append_to_scores(final_scores, run_id,scores, kappa, date ):
    classifier = run_id.split("_")[0]
    dataset= run_id.split("_")[1]
    feat_selection = run_id.split("_")[2]

    final_scores.append((classifier,dataset,str(feat_selection),run_id, '0.0',scores['0.0']["precision"],scores['0.0']["recall"],scores['0.0']["f1-score"],0.0, date )  )
    final_scores.append((classifier,dataset,str(feat_selection),run_id, '1.0',scores['1.0']["precision"],scores['1.0']["recall"],scores['1.0']["f1-score"],0.0, date )  )
    final_scores.append((classifier,dataset,str(feat_selection),run_id, 'ALL_AVG_MACRO', scores['macro avg']["precision"],scores['macro avg']["recall"],scores['macro avg']["f1-score"],kappa, date)  )
    final_scores.append((classifier,dataset,str(feat_selection),run_id, 'ALL_AVG_MICRO', scores['micro avg']["precision"],scores['micro avg']["recall"],scores['micro avg']["f1-score"],kappa, date)  )
    final_scores.append((classifier,dataset,str(feat_selection),run_id, 'ALL_AVG_WEIGHTED', scores['weighted avg']["precision"],scores['weighted avg']["recall"],scores['weighted avg']["f1-score"],kappa, date)  )

    return final_scores     



def append_to_final_data(final_data, labels, gt, classifier, run_id, date):
    
    temp_gt = []
    for pred in labels:
        cluster_id = pred[0]
        g_t = int(gt[int(float(cluster_id))])
        temp_gt.append(g_t)
        label = int(pred[1])
        atrib = ""
        
        if g_t == label and g_t == 1:
            atrib = "TP"
        elif g_t == label and g_t == 0:
            atrib = "TN"
        elif g_t == 1 and label == 0:
            atrib = "FN" 
        elif g_t == 0 and label == 1:
            atrib = "FP"
            
        final_data.append((cluster_id,classifier, run_id,g_t, label,atrib, date))

#    print(confusion_matrix(temp_gt ,labels[:,1].flatten()).ravel() )
    return final_data



# os argumentos final_data e final_scores guardam o resultaodo dos algoritmos para posteriormente ser guardado num csv
# o argumento force_search força a procura dos melhores hyperparamentros, caso ja tenham sido encontrados num execução anterior
# o argumento satellite indica de que satélite são os dados: "s1" (Sentinel-1) , "s2" (Sentinel-2) ou "s2_s1" (ambos)
# o argumento se for true apenas usa os dados de uma data e não da séries temporais
def run(folder, fgc_type, dataset, feat_selection, final_data, final_scores, force_search, satellite, static, date):
    
    print("Reading data...       DATASET",dataset  )
    gt,data_estradas,labels = DataExtration.get_data(folder,fgc_type,dataset,satellite, static, True, date)
#    gt_t,data_estradas_t,labels_t = DataExtration.get_data(folder,fgc_type,dataset,satellite, static, False, date) ######## apagar tirar o true /false
    
    if feat_selection:
        data_estradas = DataExtration.feature_selection(data_estradas, labels[:,1]) # usando lasso 
     
        
    data = data_estradas.values
#    data_t = data_estradas_t.values ### 
    
    
    print("Running Classifier")#
    ## treino em 12 set e correr no resto do ano
#    X_r =data 
#    Y_r =labels
#    X_t = data_t
#    Y_t = labels_t
    ###
    X_r,X_t,Y_r,Y_t = train_test_split(data, labels, test_size=0.33, shuffle = False) 
    
    if static:
        analysis_type = "estatico"
    else:
        analysis_type = "temporal"
    
    all_params = {}
    if os.path.isfile(folder+"/best_params/dataset_"+str(dataset)+"_"+satellite+"_"+analysis_type+".txt") :
        with open(folder+"/best_params/dataset_"+str(dataset)+"_"+satellite+"_"+analysis_type+".txt", 'r') as f:
            s = f.read()
            all_params = ast.literal_eval(s)
            
    
    best_params = {}
#    print("RAW DATA")
    pred,scores,params = cross_validation( data_estradas.columns, "RF",X_r,X_t,Y_r,Y_t, dataset, all_params,force_search)
    best_params["RF"] = params
    final_data = append_to_final_data(final_data, pred,gt ,"RF","RF_"+str(dataset)+"_"+str(feat_selection), date )
    kappa = cohen_kappa_score(Y_t[:,1],pred[:,1].ravel())
    final_scores = append_to_scores(final_scores, "RF_"+str(dataset)+"_"+str(feat_selection), scores, kappa, date )
#  
    pred,scores,params = cross_validation(data_estradas.columns, "XGB",X_r,X_t,Y_r,Y_t, dataset, all_params,force_search)
    best_params["XGB"] = params
    final_data = append_to_final_data(final_data, pred,gt ,"XGB","XGB_"+str(dataset)+"_"+str(feat_selection), date )
    kappa = cohen_kappa_score(Y_t[:,1],pred[:,1].ravel())
    final_scores = append_to_scores(final_scores, "XGB_"+str(dataset)+"_"+str(feat_selection), scores, kappa, date )    

    X_rn = preprocessing.normalize(X_r)
    X_tn = preprocessing.normalize(X_t)
    datan = preprocessing.normalize(data_estradas)
#    
    pred,scores,params = cross_validation( None, "SVM" ,X_rn,X_tn,Y_r,Y_t, dataset, all_params,force_search)
    best_params["SVM"] = params
    final_data = append_to_final_data(final_data, pred,gt ,"SVM", "SVM_"+str(dataset)+"_"+str(feat_selection), date)
    kappa = cohen_kappa_score(Y_t[:,1],pred[:,1].ravel())
    final_scores = append_to_scores(final_scores, "SVM_"+str(dataset)+"_"+str(feat_selection), scores, kappa, date ) 
#    
#    
    pred,scores,params = cross_validation( None,"KNN",X_rn,X_tn,Y_r,Y_t, dataset, all_params,force_search)
    best_params["KNN"] = params
    final_data = append_to_final_data(final_data, pred,gt ,"KNN","KNN_"+str(dataset)+"_"+str(feat_selection), date )
    kappa = cohen_kappa_score(Y_t[:,1],pred[:,1].ravel())
    final_scores = append_to_scores(final_scores, "KNN_"+str(dataset)+"_"+str(feat_selection), scores, kappa, date )
    
    #Save best hyperparameters to file 
    if not os.path.isdir(folder+"/best_params"):
        os.mkdir(folder+"/best_params")
    

    with open(folder+"/best_params/dataset_"+str(dataset)+"_"+satellite+"_"+analysis_type+".txt", "w") as f:
        f.write( str(best_params) )
             
    return final_data, final_scores


#Executa todos os classificadores com todos os datasets, e guarda todos os dados em dois ficheiros
def start(fgc_dir, fgc_type, date):

    final_data = []
    final_data.append(("CLUSTER_ID", "CLASSIFIER","RUN_ID","GROUND_TRUTH", "PREDICTED", "ATRIBUTION", "DATE"))
     
    scores = []
    scores.append(("CLASSIFIER", "DATASET", "FEATURE_SELECTION", "RUN_ID", "CLASS","PRECISION", "RECALL", "F1", "KAPPA", "DATE"))
         
    
    folder = fgc_dir+"/"+fgc_type
    
#    satellite = "s2"
#    satellite = "s1"
    satellite = "s2_s1"
    
    static = False # if Flase uses the temporal data if true only data from one date 
    
    final_data, scores = run(folder,fgc_type,1,False,final_data, scores, False,satellite,static, date)
    final_data, scores=  run(folder,fgc_type,2,False,final_data, scores, False, satellite,static, date)    
    final_data, scores = run(folder,fgc_type,3,False,final_data, scores, False, satellite,static, date)    
#    #####final_data, scores = run(folder,fgc_type,4,False,final_data, scores, False, satellite,static)    
    final_data, scores = run(folder,fgc_type,5,False,final_data, scores, False, satellite,static,date)
    
    
    print("==========================================")
    print("WITH FEATURE SELECTION")
    print("==========================================")   
    #    
    #final_data, scores = run(folder,fgc_type,1,True,final_data, scores, False, satellite,static)
    #final_data, scores = run(folder,fgc_type,2,True,final_data, scores, False, satellite,static)    
    #final_data, scores = run(folder,fgc_type,3,True,final_data, scores, False, satellite,static)    
    ##final_data, scores = run(folder,fgc_type,4,True,final_data, scores, False, satellite,static)    
    #final_data, scores= run(folder,fgc_type,5,True,final_data, scores, False, satellite,static)  
    
    
    if not os.path.isdir(folder+"/results"):
        os.mkdir(folder+"/results")

    if static:
        analysis_type = "estatico"
    else:
        analysis_type = "temporal"
        
##    with open(folder+"/results/estatico/resultados_matrix_"+analysis_type+"_"+satellite+"_"+str(date)+".csv",'w',newline='') as out:
    with open(folder+"/results/resultados_matrix_"+analysis_type+"_"+satellite+".csv",'w',newline='') as out:
        csv_out=csv.writer(out)
        for row in final_data:
            csv_out.writerow(row) 
    
    
#    with open(folder+"/results/estatico/resultados_classifers_"+analysis_type+"_"+satellite+"_"+str(date)+".csv",'w',newline='') as out:
    with open(folder+"/results/resultados_classifers_"+analysis_type+"_"+satellite+".csv",'w',newline='') as out:
        csv_out=csv.writer(out)
        for row in scores:
            csv_out.writerow(row) 
#    

    
if __name__ == '__main__': 
    
    
    if len(sys.argv) != 3 :
        print("Missing arguments! Command usage:")
        print("  python DataExtraction.py '<path_to_fgci_directory>' <type_of_fgci> \n")
        print("    type_of_fgci: 'estradas' or 'linhas'")

        sys.exit()
    
    fgci_dir = sys.argv[1]
    fgc_type = sys.argv[2]

#    dates=[20180130,20180321,20180619,20180729,20180808,20180818,20181027,20181116,20181206,20180224,20180326,20180425,20180505,20180515,20180624,20180823,20180922,20181002,20181231]
    dates=[0] #para a analise temporal
    
    
    fgc_dir = "../Mação/"
    fgc_type = "estradas"
    for date in dates:
        start(fgc_dir, fgc_type, date)
    
    
