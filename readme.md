# README

### EXECUTION ENVIRONMENT
All the development was done on a laptop with the windows 10 operating system. The characteristics of the computer are the following:

* **CPU:** Intel Core i7-8750H Hexa Core, 2.20 GHz up to 4.10 GHz
* **DISCO:** SSD 256GB NVMe PCIe + 1TB HDD
* **RAM:** 16GB DDR4 2666MHz
* **GPU:** NVIDIA GeForce GTX 1060 6GB GDDR5

The implementation was done in python, through Anaconda. All used libraries are available in Anaconda and as such are of easy installation.

Libraries used:

*	qgis (https://anaconda.org/conda-forge/qgis)
	* contains libraries for the manipulation of vector files and raster images
*	gdal (https://anaconda.org/conda-forge/gdal)
	* raster file handling
*	rasterio (https://anaconda.org/conda-forge/rasterio)
	* raster file handling
*	fiona (https://anaconda.org/conda-forge/fiona)
	* vector file handling
*	xgboost (https://anaconda.org/conda-forge/xgboost)
	* machine learning algorithm that uses parallelization via the CPU or GPU
*	cudatoolkit (https://anaconda.org/anaconda/cudatoolkit)
	* Required for xgboost and requires installation of CUDA (https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html)
	
Since some of the scripts use parallelization to improve runtimes, **all scripts must be executed in the command line**.

### DATA
*	Data used is available
	* https://drive.google.com/drive/folders/1X2BiWxDOFhNWEgXmbaDJC4DTNX3Crmj_?usp=sharing


### SCRIPTS

* SAR_Preprocessing
	* Contains a .bat script that performs the pre-processing of Sentinel-1 products. The steps of the pre-processing are described in the file *SAR_preprocessing_graph.xml* that was generated using the SNAP tool (https://step.esa.int/main/toolboxes/snap/). The information on how to run the script is in the *Readme.txt* file.

*	concavehull
	* Contains the files needed to run the concavehull algorithm. Implementation of the algorithm used: https://github.com/sebastianbeyer/concavehull

*	*ProcessFGCFiles.py*
	* Performs all pre-processing of FGCI *shapefiles*:
		* separation of the different types of lanes (roads, power lines and houses)
		* creation of *buffers* outside the 20m bands
		* removal of areas that overlap one another
		* division of the tracks into sections of approximately the same dimension (uses the *CreateSections.py script*)
		* intersection of the sections with the FGCI and the *buffers*, saving the result

*	*CreateSections.py*
	* Divides the bands into sections of approximately the same size. Use the algorithm *ConcaveHull.py* 

*	*Geometries_FGC.py*
	* Contains several useful functions for handling shapefiles (intersections, cutouts, couplings, *buffers*, etc.)
	
*	*ExtractClustersData.py*
	* Performs the extraction of information from satellite images (Sentinel-1 and Sentinel-2), for each of the sections (*buffer* and inside FGCI)
	* There is a verification if the images have been previously aligned, re-sampled and if the vegetation indexes have already been calculated, performing any pre-processing that is missing.

*	*EstimateIntervention.py*
	* Estimate the date of intervention of some sections along the road lanes.

*	*DataExtration.py*
	* Using the images of the sections that were extracted with *ExtractClustersData.py script*, performs the metric (min, max, mean, standard deviation) extraction of the various images, per section, and saves the result in .csv files. Of all the pre-processing and data extraction steps this is the one that takes the most time.

*	*FullProcessing.py*
	* Performs the entire processing chain required to analyze the FGCI. This consists of the sequential execution of some of the above mentioned scripts.
	
*	*AdjustGeo.py*
	* Script not used in the processing chain, but which was used to perform the initial alignments of the satellite images of the reference image (information of the displacements made is in the file *shifts.txt* ). It can be useful if you need to "manually" adjust the geographical position of some image. 
	
*	*imageAlign.py*
	* Performs the alignment of new satellite images, based on the reference image created. **It is necessary to change the path of the reference image, which is in the script.**

*	*resampleTo10m.py*
	* Performs the resampling of some images that have spatial resolution from 20m to 10m. This is because some vegetation indexes use these images, being necessary the resampling to make the calculations.

*	*Supervised.py*
	*Machine Learning algorithms using different data sets and combinations (requires shapefiles with *ground truth*.
	
*	*Unsupervised.py*
	* Run the *KMeans* algorithm with the data from the lanes along the roads, writing the result of the grouping in a vector file.

*	*ThresholdClassifier.py*
	* Carries out a classification of the bands using time series of vegetation indices and the imposition of a limit.


### EXECUTION


Before extracting the data from the satellite images (i.e. before running *ExtractClustersData.py* or *FullProcessing.py*) it is necessary to preprocess the images from Sentinel-1 using *script.bat*.

The complete processing of the vector information of the FMZs of a given Municipality is dependent on this file having been correctly created by the Municipality. In some cases it may not be possible to prepare and analyze the vectorial information of the bands around houses.

For the specific case of Mação there are files that contain the ground truth (inside the Ground_Truth folder), which should replace previously generated files. After the replacement the *ExtractClustersData.py* and *DataExtration.py* scripts should be executed again.

All the extracted information is stored in folders, inside the directory where the *shapefile* of the FGCI of the council that is being analyzed is found.

